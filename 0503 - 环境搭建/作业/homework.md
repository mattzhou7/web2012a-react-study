## 复习以下内容

建立好环境：

- 如何创建react app环境
- 建立好git环境
- 分支使用

重点知识

- 如何使用ReactDOM渲染挂载点
- JSX和createElement函数的互相转换

## 预习

预习以下内容

1.Hello World.html
2.JSX 简介.html
3.元素渲染.html

要求：

- 运行文档中的例子
- 做笔记
